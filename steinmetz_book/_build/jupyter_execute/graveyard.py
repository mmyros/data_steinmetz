# Graveyard

if 0:
    # HIDE CELL
    import deps
    from importlib import reload
    reload(deps)

    alldat, dat_LFP, dat_ST = deps.get_data()
    dat = alldat[1]
    sp, neu, trial_starts = deps.convert_raster_to_spiketimes(dat)
    events_df = deps.convert_events_to_dataframe(dat, trial_starts,
                                            event_names=('gocue', 'response_time','feedback_time'),
                                            condition_names=('response','contrast_right','contrast_left','feedback_type'))

    spykes_times = deps.spykes_get_times(sp, neu)

if 0:
    for event_name in events_df.columns[events_df.columns.str.contains('time')]:
        for condition_name in events_df.columns[~events_df.columns.str.contains('time')]:
            1
    _, all_psth = deps.get_psth(spikes=spykes_times,
                       spykes_df=events_df,
                       event=event_name, 
                       conditions=condition_name,
                       window=[-500, 1500], 
                       bin_size=10,
                                #fr_thr=0,
                       )
    # Make psth
    xar=deps.spyke2xar(all_psth)

if 0:
    reload(deps)
    # Cluster
    df,df_tidy,index=deps.cluster(xar,plotose=False)

    #df = xar.to_dataframe().reset_index()

if 0:
    import seaborn as sns
    import matplotlib.pylab as plt
    from matplotlib import colors
    plt.figure(figsize=(20,20))
    sns.heatmap(df_tidy, robust=True,
            center=True,
            #norm=colors.SymLogNorm(0.03),
            );

if 0:
    # Altair
    import altair as alt
    df['Neuron']=df['Neuron']
    alt.data_transformers.disable_max_rows()
    chart=alt.Chart(df).mark_rect().encode(
        x='Time:O',
        y='Neuron:O',
        color=alt.Color(f'{xar.name}:Q',scale=alt.Scale(scheme='purpleorange'),)
    ).properties(height=550,width=700)#.facet(column=condition_name)
    chart

if 0:
    # Altair
    import altair as alt
    alt.data_transformers.disable_max_rows()
    chart=alt.Chart(df).mark_square().encode(
        x='Time:O',
        y='Neuron:O',
        color=alt.Color(f'{xar.name}:Q',scale=alt.Scale(scheme='purpleorange'),)
    ).properties(height=550,width=400).facet(column=condition_name)
    chart

if 0:
    from holoviews.operation.datashader import rasterize
    import numpy as np
    import pandas as pd
    import holoviews as hv

    from bokeh.plotting import show
    import holoviews as hv
    import hvplot.pandas  # noqa
    %output size=320


    #df['time_feedback']=df['time_feedback']+df['time_feedback'].min()+1
    #rasterize(hv.Image(df_tidy.values,['Time','Neuron'],'time_feedback'), dynamic=True)
    #rasterize(hv.Image(df_tidy,['Time','Neuron'],'time_feedback'), dynamic=True)
    #rasterize(hv.QuadMesh(df_tidy,['Time','Neuron'],'time_feedback'), dynamic=True)
    #dfz=df_tidy.xs(1,level='feedback_type')
    dfz=df_tidy.copy()
    #dfz[dfz>4]=4
    #dfz+dfz.min().min()
    rasterize(hv.Image((df_tidy.columns.values,range(df_tidy.shape[0]), df_tidy.values)
    ).options(invert_yaxis=True,cmap='Plasma',logz=False),
              dynamic=False)
    #dfz.min()
    hv.Image((df_tidy.columns.values,range(df_tidy.shape[0]), df_tidy.values)
            ).options(invert_yaxis=True,cmap='Plasma')
    hv.HeatMap(df,['Time','Neuron'],'time_feedback')


import numpy as np
import pandas as pd
import holoviews as hv

from scipy.cluster.hierarchy import linkage, dendrogram

hv.notebook_extension('bokeh')
%opts Path.Dendrogram (line_color='black') [xaxis=None yaxis=None show_grid=False show_title=False show_frame=False border=0]

def compute_linkage(dataset, dim, vdim):
    arrays, labels = [], []
    for k, v in dataset.groupby(dim, container_type=list, group_type=hv.Dataset):
        labels.append(k)
        arrays.append(v.dimension_values(vdim))
    X = np.vstack(arrays)
    X = np.ma.array(X, mask=np.logical_not(np.isfinite(X)))
    Z = linkage(X)
    ddata = dendrogram(Z, labels=labels, no_plot=True)
    ddata['mh'] = np.max(Z[:, 2])
    return ddata

def get_dendrogram(dataset, dim, vdim):
    if not isinstance(dim, list): dim = [dim]
    kdims = dataset.kdims
    dataset = hv.Dataset(dataset)
    sort_dims, dendros = [], []
    for i, d in enumerate(dim):
        ddata = compute_linkage(dataset, d, vdim)
        mh = ddata['mh']
        order = [ddata['ivl'].index(v) for v in dataset.dimension_values(d)][::-1]
        sort_dim = 'sort%s' % i
        dataset = dataset.add_dimension(sort_dim, 0, order)
        sort_dims.append(sort_dim)
        ivw = len(ddata['ivl']) * 10
        dvw = mh + mh * 0.05
        extents = (0, 0, ivw, dvw)
        dendro = hv.Path(zip(ddata['icoord'], ddata['dcoord']), kdims=['a', 'b'], group='Dendrogram',
                         extents=extents)
        dendros.append(dendro)
    opts = [dict(width=80), dict(height=80)]
    if len(sort_dims) == 1: sort_dims = kdims[:1] + sort_dims
    dataset = hv.HeatMap(dataset.sort(sort_dims).reindex(kdims))
    for dendro, opt in zip(dendros, opts):
        dataset = dataset << dendro(plot=opt)
    return dataset

%opts Layout [shared_axes=False] Path {+axiswise} HeatMap [border=0]
ndoverlay = hv.NdOverlay({i: hv.Curve([(chr(65+j), np.random.rand()) for j in range(10)])
                          for i in range(5)}, kdims=['z'])
dataset = hv.Dataset(ndoverlay.dframe(), vdims=['y'])

get_dendrogram(dataset, ['x', 'z'], 'y')


# brain area
if 0:
    dat['brain_area']

    _, all_psth = deps.get_psth(spikes=spykes_times,
                       spykes_df=events_df,
                       event=event_name, 
                       conditions=condition_name,
                       window=[-500, 1500], 
                       bin_size=10,
                                fr_thr=0,
                       )

    # Make psth
    xar=deps.spyke2xar(all_psth)
    import seaborn as sns
    value_name = xar.name
    condition_name = xar.dims[0]
    df = xar.to_dataframe().reset_index()
    df.shape,dat['brain_area'].shape
    #xar['brain_area']=dat['brain_area']

    dat_ST

    all_psth['data'][0].shape

    xar

    df = df.pivot(index=['Neuron', condition_name],
                  columns='Time',
                  values=value_name)

    # Cluster conditions together, plot them (also together)
    g = sns.clustermap(df,
                       z_score=False,
                       figsize=[20, 20],
                       col_cluster=False,
                       # standard_scale=1,
                       metric="correlation",
                       center=True,
                       #row_colors=dat['brain_area'], #TODO
                       robust=True)

if 0:
    # Plot using sns
    value_name=xar.name
    for condition in df[condition_name].unique():
        # Pivot if necessary
        df1=df.pivot(index=['Neuron',condition_name], 
                                              columns='Time',
                                              values=value_name)
        deps.heatmap(df1.xs(condition,level=condition_name));plt.show()    

## Examples of datashading from holoviews
if 0:
    from holoviews.operation.datashader import rasterize
    import numpy as np
    import pandas as pd
    import holoviews as hv

    from bokeh.plotting import show
    import holoviews as hv
    import hvplot.pandas  # noqa
    %output size=320


    a, b = np.random.randn(1000, 2).T
    df = pd.DataFrame({'a': a*10, 'b': b}, columns=['a', 'b'])
    rasterize(hv.Scatter(df), width=10, height=10, dynamic=False)

    from holoviews.operation.datashader import rasterize

    a, b = np.random.randn(1000, 2).T
    dfr = pd.DataFrame({'a': a*10, 'b': b}, columns=['a', 'b'])
    rasterize(hv.Scatter(dfr), width=10, height=10, dynamic=False)

    dfr