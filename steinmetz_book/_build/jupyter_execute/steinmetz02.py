# Hierarchical clustering for session 2


## Make spiketimes and events dataframe


# HIDE CODE
import deps
from importlib import reload
reload(deps)

alldat, dat_LFP, dat_ST = deps.get_data()
dat = alldat[2]
#sp, neu, trial_starts = deps.convert_raster_to_spiketimes(dat)

dat_st=dat_ST[2]['ss']
sp, neu, trial_starts = deps.concat_spiketimes(dat_st)
reload(deps)

events_df = deps.convert_events_to_dataframe(dat, trial_starts,
                                        event_names=('gocue', 'response_time','feedback_time'),
                                        condition_names=('response','contrast_right','contrast_left','feedback_type'))

spykes_times = deps.spykes_get_times(sp, neu, thr_n_spikes=55)
spykes_times = deps.spykes_add_brain_regions(spykes_times, dat['brain_area'])

## Make and plot clusters

# HIDE CODE
# Hvplot
from bokeh.plotting import show
import holoviews as hv
import hvplot.pandas  # noqa
%output size=320

#!pip install -U hvplot
reload(deps)
for event_name in events_df.columns[events_df.columns.str.contains('time')]:
    for condition_name in events_df.columns[~events_df.columns.str.contains('time')]:
        print(f"======= {event_name} / {condition_name} =======")
        _, all_psth = deps.get_psth(spikes=spykes_times,
                               spykes_df=events_df,
                               event=event_name, 
                               conditions=condition_name,
                               window=[-500, 800], 
                               bin_size=15,
                               )
        # Make psth
        xar=deps.spyke2xar(all_psth,
                           spykes_times['brain_group'].values,  
                           spykes_times['brain_group_color'].values)

        # Cluster
        df,df_tidy,index=deps.cluster(xar,plotose=False)
        
        if df is None:
            continue
            
        
        # Plot hvplot by condition
        plot=df.hvplot.heatmap(x='Time',y='Neuron',C=xar.name,row=condition_name,logz=False,
                          flip_yaxis=True,cmap='Plasma',
                          colorbar=False,symmetric=False,grid=False,xaxis=False,yaxis=False)
                          #,datashade=True)
        #show(hv.render(plot))
        deps.hv_render_png(plot)

#deps.hv_render_png(plot)
