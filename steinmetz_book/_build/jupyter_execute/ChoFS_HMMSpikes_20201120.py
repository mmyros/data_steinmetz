
## Frances Cho's code for implementing a Poisson Hidden Markov Model (from Scott Linderman's SSM package) to infer latent states from neural spiking data obtained during a visual discrimination task in mice (data from Steinmetz et al., 2019). This code was used for a group project during Neuromatch Academy 2020, and was used to generate part of the results presented at the Bernstein Conference 2020. Group project members included Frances Cho, Maxym Myroshnychenko, Tanvi Rajan, Sunandha Srikanth, and Jun Ye. 

## Import Steinmetz Dataset

# !pip install --upgrade nbformat

# Data retrieval
import os, requests

fname = []
for j in range(3):
  fname.append('steinmetz_part%d.npz'%j)
url = ["https://osf.io/agvxh/download"]
url.append("https://osf.io/uv3mw/download")
url.append("https://osf.io/ehmw2/download")

for j in range(len(url)):
  if not os.path.isfile(fname[j]):
    try:
      r = requests.get(url[j])
    except requests.ConnectionError:
      print("!!! Failed to download data !!!")
    else:
      if r.status_code != requests.codes.ok:
        print("!!! Failed to download data !!!")
      else:
        with open(fname[j], "wb") as fid:
          fid.write(r.content)

        
# Data loading
import numpy as np

alldat = np.array([])
for j in range(len(fname)):
  alldat = np.hstack((alldat, np.load('steinmetz_part%d.npz'%j, allow_pickle=True)['dat']))



# select just one of the recordings here. 11 is nice because it has some neurons in vis ctx. 
dat = alldat[15]
print(dat.keys())




# Dependencies for HMM (github.com/slinderman/ssm)

!pip install git+https://github.com/slinderman/ssm.git
import autograd.numpy as np
import autograd.numpy.random as npr
npr.seed(0)

import ssm
from ssm.util import find_permutation
from ssm.plots import gradient_cmap, white_to_color_cmap
from ssm import model_selection


import matplotlib.pyplot as plt
%matplotlib inline

import seaborn as sns
sns.set_style("white")
sns.set_context("talk")

color_names = [
    "ocean blue",
    "golden rod",
    "robin's egg blue",
    "greenish",
    "purplish blue",
    "burgundy"
    ]

colors = sns.xkcd_palette(color_names)
cmap = gradient_cmap(colors)

# Specify whether or not to save figures
save_figures = True

# Other dependencies
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_validate
from sklearn.naive_bayes import BernoulliNB

from joblib import dump, load
import pickle



# Select neurons based on GLM weights

# Jun and Tanvi's code to extract relevant neurons in each of 5 brain regions

# '''
# Make nsession x 5 brain regions. Each element is vector of which neurons are in the brain region
# Neurons are selected based on coupling weights obtained from Tanvi's GLM
# '''

regions = ["vis ctx", "thal", "hipp", "other ctx", "midbrain", "basal ganglia", "cortical subplate", "other"]
brain_groups = [["VISa", "VISam", "VISl", "VISp", "VISpm", "VISrl"], # visual cortex
                ["CL", "LD", "LGd", "LH", "LP", "MD", "MG", "PO", "POL", "PT", "RT", "SPF", "TH", "VAL", "VPL", "VPM"], # thalamus
                ["CA", "CA1", "CA2", "CA3", "DG", "SUB", "POST"], # hippocampal
                ["ACA", "AUD", "COA", "DP", "ILA", "MOp", "MOs", "OLF", "ORB", "ORBm", "PIR", "PL", "SSp", "SSs", "RSP"," TT"], # non-visual cortex
                ["ACB", "CP", "GPe", "LS", "LSc", "LSr", "MS", "OT", "SNr", "SI"], # basal ganglia 
                ["BLA", "BMA", "EP", "EPd", "MEA"] # cortical subplate
                ]
f=plt.figure(figsize=(15,5));
neurons_for_decoding = np.empty(shape=(39,5),dtype='object');
col_list = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
threshold = 33 
for iregion in range(5):
    # Plot placeholders for label
    plt.plot(np.inf,np.inf,color=col_list[iregion], label=regions[iregion]); 
    for isession in range(39):
        dat = alldat[isession];
        brain_area = dat['brain_area'];
        neurons_for_decoding[isession, iregion] = np.zeros(shape=(len(brain_area),),dtype='int')
        for i_small_region, small_region in enumerate(brain_groups[iregion]):
            kk = np.where(brain_area == small_region)[0]; 
            neurons_for_decoding[isession, iregion][kk] = 1;
        if np.sum(neurons_for_decoding[isession,iregion]) < threshold:
            neurons_for_decoding[isession,iregion] = np.zeros(shape=(len(brain_area),),dtype='int') 
        total = 0
        for i in range(iregion):
            total += np.sum(neurons_for_decoding[isession, i])
        plt.bar(isession, np.sum(neurons_for_decoding[isession,iregion]), bottom = total, color=col_list[iregion]);
plt.xlabel('Session #'); 
plt.ylabel('Number of neurons in each brain region');
plt.legend(); 
plt.show();

# Fit HMM for each session and brain region

sall_spks = {}
sall_choice = {}
sall_reward = {}
sall_hmm_list= {}
sall_lls = {}
sall_ML_states= {}
sall_contrast_l ={}
sall_contrast_r ={}
sall_posterior = {}

for i in range(0,len(alldat)):
    
    print('/n------ Session: ', i)

    all_spks = []
    all_choice = []
    all_reward = []
    all_hmm_list=[]
    all_lls = []
    all_ML_states=[]
    all_contrast_l=[]
    all_contrast_r=[]
    all_posterior=[]
    
    
    for iregion in range(5):
                
        print('---- Region: ', regions[iregion])
        
        # grab indices of neurons in 5 brain regions of interest 
        curr_neurons = np.array(neurons_for_decoding[i][iregion])
        curr_neurons_idx = list(np.where(curr_neurons == 1))[0]

        print('# of neurons in region: ', np.sum(curr_neurons))
        print('# of neurons in session: ' , len(curr_neurons))

        
        if sum(curr_neurons_idx) == 0:
            print('Not enough neurons in region.\n')
            
            # keep as placeholders
            dummy = np.empty((2,2,2)); dummy[:] = np.nan
            
            all_choice.append(dummy)
            all_reward.append(dummy)
            all_spks.append(dummy)
            all_contrast_r.append(dummy)
            all_contrast_l.append(dummy)
            all_lls.append(dummy)
            all_hmm_list.append(dummy)
            all_ML_states.append(dummy)
            all_posterior.append(dummy)
            
            pass
        
        else:
            #### format data
            newdata = np.array(alldat[i]['spks'])[curr_neurons_idx]
            newchoice = np.array(alldat[i]['response'])
            newreward = np.array(alldat[i]['feedback_type'])
            contrast_r = np.array(alldat[i]['contrast_right'])
            contrast_l = np.array(alldat[i]['contrast_left'])
            train_data = np.swapaxes(newdata,1,2) # swap axes
            train_obs=np.reshape(train_data, ((train_data.shape[0],-1))); train_obs = train_obs.T


            # save choice, reward, visual contrast values, and spikes from all trials within session
            all_choice.append(newchoice)
            all_reward.append(newreward)
            all_contrast_r.append(contrast_r)
            all_contrast_l.append(contrast_l)
            all_spks.append(newdata)
            

            #### Initialize, fit, and cross-validate Poisson HMM
            # Set hyperparameters
            N_iters = 20
            num_states = 3 
            obs_dim = train_obs.shape[1] # # of observations aka # of neurons

            # Fit and keep model for each session/brain region
            hmm = ssm.HMM(num_states, obs_dim, observations="poisson")
            dump(hmm, 'HMM_session%d_%d.joblib'%(i,iregion))
            hmm_lls = hmm.fit(train_obs, method="em", num_iters=N_iters) # get losses
            all_lls.append(hmm_lls); all_hmm_list.append(hmm)
            post = hmm.filter(train_obs) # get posterior
            all_posterior.append(post)
            

            #### Find most likely states 
            most_likely_states = hmm.most_likely_states(train_obs)
            all_ML_states.append(most_likely_states)
            
            
    # Append all brain regions from each session
    sall_spks[i] = all_spks
    sall_choice[i] = all_choice
    sall_reward[i] = all_reward
    sall_hmm_list[i]= all_hmm_list
    sall_lls[i] = all_lls
    sall_ML_states[i] = all_ML_states
    sall_contrast_l[i] = all_contrast_l
    sall_contrast_r[i] = all_contrast_r
    sall_posterior[i] = all_posterior


# # Save all as pickles
# f = open('sall_spks.pckl', 'wb'); pickle.dump(sall_spks,f); f.close()
# f = open('sall_choice.pckl', 'wb'); pickle.dump(sall_choice,f); f.close()
# f = open('sall_reward.pckl', 'wb'); pickle.dump(sall_reward,f); f.close()
# f = open('sall_hmm_list.pckl', 'wb'); pickle.dump(sall_hmm_list,f); f.close()
# f = open('sall_ML_states.pckl', 'wb'); pickle.dump(sall_ML_states,f); f.close()
# f = open('sall_lls.pckl', 'wb'); pickle.dump(sall_lls,f); f.close()
# f = open('sall_contrast_r.pckl', 'wb'); pickle.dump(sall_contrast_r,f); f.close()
# f = open('sall_contrast_l.pckl', 'wb'); pickle.dump(sall_contrast_l,f); f.close()

# print('Saved Session')
# print('')



# Plot inferred states

# Reformat 
def formatHMMplot(inferred_states):
    
    trial_length = 250    
    trial_n = int(len(inferred_states)/trial_length)
    inf_reshape = (np.reshape(inferred_states, (trial_length,-1))).T                          

    X = inf_reshape
    return X


cd /Users/fsc/HMM_Bernstein_September2020/20201120_HMMPlots_Regions

# # # # # #
# # Plot! 
# # # # # # 


for i in range(0,len(alldat)):
    print('------session: ', i)
    for iregion in range(5):
        inf = sall_ML_states[i][iregion]
        if np.isnan(inf).any():
            pass
        else:
            
            inf2 = formatHMMplot(inf)
            
            # get choice, reward, go/no-go for each trial
            choice=sall_choice[i][iregion]
            reward=sall_reward[i][iregion]
            right_contrast=alldat[i]['contrast_right']
            right_contrast_sort = np.array(right_contrast)
            choice_sort = np.array(choice)
            choice_sort[choice_sort==-1]=1

            # get index to mark boundary between trial types in plots
            ng_idx = np.sum(choice==0)
            go_idx = len(choice) - ng_idx
            right_none_idx = np.sum(right_contrast==0)
            reward_idx = np.sum(reward==-1)
            
            # sort trials
            isort1 = np.argsort(choice_sort).flatten() # sort by response (i.e. go/no go)
            isort2 = np.argsort(reward).flatten() # sort by reward (i.e. correct/incorrect)
            isort3 = np.argsort(right_contrast_sort).flatten() # sort by visual stimulus 
            
            # plot
            plt.figure(figsize=(15, 5))    
            plt.subplot(131)
            plt.imshow(inf2[isort1,:], aspect="auto", cmap=cmap, vmin=0, vmax=len(colors)-1)
            plt.ylabel("Trials Sorted by No-Go vs. Go")
            plt.plot([0, 249], [ng_idx, ng_idx], 'r', linewidth=3)    
            plt.plot([110, 110], [0,len(choice)], '--r', linewidth=2) # go/no-go epoch = 110 to 150 ms         
            plt.plot([150, 150], [0,len(choice)], '--r', linewidth=2)
            plt.ylim([len(choice),0])
            plt.xticks(np.arange(0,250, step=100),['0','1','2'])
            plt.xlabel('Time (sec)')
            plt.title('Session: '+str(i)+' - Region: '+str(regions[iregion]))
            plt.savefig('HMMSession%d'%i+'_GoNoGo_Region%d.png'%iregion)

            plt.subplot(132)
            plt.imshow(inf2[isort3,:], aspect="auto", cmap=cmap, vmin=0, vmax=len(colors)-1)
            plt.ylabel("Trials Sorted by Right Contrast = 0")
            plt.xlabel('Time (sec)')
            plt.plot([0, 249], [right_none_idx, right_none_idx], 'r', linewidth=3)    
            plt.plot([30, 30], [0,len(right_contrast_sort)], '--r', linewidth=2) # vis stim epoch = 30 to 70 ms
            plt.plot([70, 70], [0,len(right_contrast_sort)], '--r', linewidth=2)
            plt.ylim([len(right_contrast_sort),0])
            plt.xticks(np.arange(0,250, step=100),['0','1','2'])
            plt.xlabel('Time (sec)')
            plt.title('Session: '+str(i)+' - Region: '+str(regions[iregion]))
            plt.savefig('HMMSession%d'%i+'_VisContrast_Region%d.png'%iregion)
            
            plt.subplot(133)
            plt.imshow(inf2[isort2,:], aspect="auto", cmap=cmap, vmin=0, vmax=len(colors)-1)
            plt.ylabel("Trials Sorted by No-Reward vs. Reward")
            plt.xlabel('Time (sec)')
            plt.plot([0, 249], [reward_idx, reward_idx], 'r', linewidth=3)    
            plt.plot([50, 50], [0,len(reward)], '--r', linewidth=1) # reward epoch = 50 to 130 ms
            plt.plot([130, 130], [0,len(reward)], '--r', linewidth=1)
            plt.ylim([len(reward),0])
            plt.xticks(np.arange(0,250, step=100),['0','1','2'])
            plt.xlabel('Time (sec)')
            plt.title('Session: '+str(i)+' - Region: '+str(regions[iregion]))
            plt.savefig('HMMSession%d'%i+'_Reward_Region%d.png'%iregion)
            plt.tight_layout()


# # Plot losses for each brain region and session

# for i in range(0,len(alldat)):
#     print('------Session: ', i)
#     plt.figure(figsize=(5, 5))    

#     for iregion in range(5):
#         curr_lls = sall_lls[i][iregion]
        
#         if np.isnan(curr_lls).any():
#             pass
#         else:
#             plt.plot(curr_lls, label=regions[iregion])
#             plt.legend(loc='center left', bbox_to_anchor=(1,0.5), labels=regions[iregion])

#     plt.xlabel("EM Iteration")
#     plt.ylabel("Log Probability")
#     plt.title('Session '+str(i))
#     plt.legend(loc="lower right")
#     plt.show()



## Naives Bayes Classifier

# Reformat to decode based on entire trial 
def formatBern(inferred_states, choices, rewards):
    
    trial_length = 250    
    trial_n = int(len(inferred_states)/trial_length)
    inf_reshape = (np.reshape(inferred_states, (trial_length,-1))).T                          

    X = inf_reshape
    Y_dir = np.array(choices)
    Y_dir[Y_dir==-1]=1 # go/no go 
    Y_reward = np.array(rewards)
    
    return X, Y_dir, Y_reward


# Reformat to decode based on just Visual Stim epoch or Go/No-Go epoch
def formatBern2(inferred_states, choices, rewards, dat):
    
    trial_length = 250    
    trial_n = int(len(inferred_states)/trial_length)
    inf_reshape = (np.reshape(inferred_states, (trial_length,-1))).T             
    
    ## Grab +/- 20 ms relative to Visual Stim and Go Cue
    # slice relative to visual stimulus
    half_time_wind = 20 # half of 40 ms, aka the minimum inter-event epoch
    X_stim = np.array(inf_reshape)
    X_stim = X_stim[:,50-half_time_wind:50+half_time_wind]
    
    # slice relative to go cue
    gocue_time = (dat['gocue']*100+50).astype(int).flatten()
    half_time_wind = 20 # half of 40 ms, aka the minimum inter-event epoch
    X_all = np.array(inf_reshape)
    all_gocue_slice=[]
    for j in range(len(inf_reshape)):
        gocue_slice = X_all[j,gocue_time[j]-half_time_wind:gocue_time[j]+half_time_wind]
        all_gocue_slice.append(gocue_slice)
    all_gocue_slice = np.stack(all_gocue_slice, axis=1) 
    X_go = all_gocue_slice.T # output shape for session 5: 290trialsx40bins
    
    Y_dir = np.array(choices)
    Y_dir[Y_dir==-1]=1 # go/no go 
    Y_reward = np.array(rewards)
    
    return X_stim, X_go, Y_dir, Y_reward

# Use Naive Bayes Classifier to decode whether right-side visual stimulus was present or not
def classifyBern(X, Y_contrast):
    
    clf_go = BernoulliNB()
    clf_reward = BernoulliNB()
    scores_contrast = cross_validate(clf_go, X, Y_contrast, cv=3, return_train_score=True)

    all_score_contrast_test = scores_contrast['test_score']
    
    return all_score_contrast_test

# Use Naive Bayes Classifier to decode whether trial was Go/NoGo or Correct/Incorrect
def classifyBern2(X, Y_dir, Y_reward):
    
    clf_go = BernoulliNB()
    clf_reward = BernoulliNB()
    scores_go = cross_validate(clf_go, X, Y_dir, cv=3, return_train_score=True)
    scores_reward = cross_validate(clf_reward, X, Y_reward, cv=3, return_train_score=True)
    all_score_go_test = scores_go['test_score']
    all_score_reward_test = scores_reward['test_score']
    
    return all_score_go_test, all_score_reward_test



# # # # #
# # Note:
# # For each trial, we considered 3 distinct, behaviorally-relevant epochs
# # and used the following time bins for decoding. 
# # See Steinmetz et al., 2019 for additional details abut trial structure.
# # Visual stimulus presentation epoch = 30 to 70 ms
# # Reward epoch = 50 to 130 ms
# # Go/no-go (i.e. decision) epoch = 110 to 150 ms
# # # # #

a_visslice_all_score_go={}
a_visslice_all_score_reward={}
a_visslice_all_score_contrast={}
a_goslice_all_score_go={}
a_goslice_all_score_reward={}
a_goslice_all_score_contrast={}
a_trial_all_score_contrast={}
a_trial_all_score_go={}
a_trial_all_score_reward={}


for j in range(len(alldat)):
    s_visslice_all_score_go=[]
    s_visslice_all_score_reward=[]
    s_visslice_all_score_contrast=[]
    s_goslice_all_score_go=[]
    s_goslice_all_score_reward=[]
    s_goslice_all_score_contrast=[]
    s_trial_all_score_contrast=[]
    s_trial_all_score_go=[]
    s_trial_all_score_reward=[]

    
    for iregion in range(5):
        
        inf = sall_ML_states[j][iregion]
        if np.isnan(inf).any():
            
            dummy = np.empty(1); dummy[:] = np.nan
            s_visslice_all_score_go.append(dummy)
            s_visslice_all_score_reward.append(dummy)
            s_visslice_all_score_contrast.append(dummy)
            s_goslice_all_score_go.append(dummy)
            s_goslice_all_score_reward.append(dummy)
            s_goslice_all_score_contrast.append(dummy)
            s_trial_all_score_contrast.append(dummy)
            s_trial_all_score_go.append(dummy)
            s_trial_all_score_reward.append(dummy)

            pass
        
        else:
            # Decode Go/NoGo or Reward using either visual stimulus or go epoch
            X_stim, X_go, Y_dir, Y_reward = formatBern2(sall_ML_states[j][iregion], sall_choice[j][iregion], sall_reward[j][iregion], alldat[j])
            visslice_all_score_go, visslice_all_score_reward = classifyBern2(X_stim, Y_dir, Y_reward)
            goslice_all_score_go, goslice_all_score_reward = classifyBern2(X_go, Y_dir, Y_reward)
            
            # Decode whether visual stimulus contrast was present using either visual stimulus or go epoch
            Y_contrast = alldat[j]['contrast_right']==0
            visslice_all_score_contrast = classifyBern(X_stim, Y_contrast)
            goslice_all_score_contrast = classifyBern(X_go, Y_contrast)

            # Decode using entire trial duration
            X, Y_dum, Y_dum2 = formatBern(sall_ML_states[j][iregion], sall_choice[j][iregion], sall_reward[j][iregion])
            trial_all_score_contrast = classifyBern(X, Y_contrast)
            
            X, Y_dir, Y_reward = formatBern(sall_ML_states[j][iregion], sall_choice[j][iregion], sall_reward[j][iregion])
            all_score_go, all_score_reward = classifyBern2(X, Y_dir, Y_reward)

            
            
            s_visslice_all_score_go.append(visslice_all_score_go)
            s_visslice_all_score_reward.append(visslice_all_score_reward)
            s_visslice_all_score_contrast.append(visslice_all_score_contrast)
            s_goslice_all_score_go.append(goslice_all_score_go)
            s_goslice_all_score_reward.append(goslice_all_score_reward)
            s_goslice_all_score_contrast.append(goslice_all_score_contrast)
            s_trial_all_score_contrast.append(trial_all_score_contrast)
            s_trial_all_score_go.append(all_score_go)
            s_trial_all_score_reward.append(all_score_reward)


    a_visslice_all_score_go[j] = s_visslice_all_score_go
    a_visslice_all_score_reward[j] = s_visslice_all_score_reward
    a_visslice_all_score_contrast[j] = s_visslice_all_score_contrast
    a_goslice_all_score_go[j] = s_goslice_all_score_go
    a_goslice_all_score_reward[j] = s_goslice_all_score_reward
    a_goslice_all_score_contrast[j] = s_goslice_all_score_contrast
    a_trial_all_score_contrast[j] = s_trial_all_score_contrast
    a_trial_all_score_go[j] = s_trial_all_score_go
    a_trial_all_score_reward[j] = s_trial_all_score_reward

    

# # Save all as pickles
# f = open('a_visslice_all_score_go.pckl', 'wb'); pickle.dump(a_visslice_all_score_go,f); f.close()
# f = open('a_visslice_all_score_reward.pckl', 'wb'); pickle.dump(a_visslice_all_score_reward,f); f.close()
# f = open('a_visslice_all_score_contrast.pckl', 'wb'); pickle.dump(a_visslice_all_score_contrast,f); f.close()
# f = open('a_goslice_all_score_go.pckl', 'wb'); pickle.dump(a_goslice_all_score_go,f); f.close()
# f = open('a_goslice_all_score_reward.pckl', 'wb'); pickle.dump(a_goslice_all_score_reward,f); f.close()
# f = open('a_goslice_all_score_contrast.pckl', 'wb'); pickle.dump(a_goslice_all_score_contrast,f); f.close()
# f = open('a_trial_all_score_contrast.pckl', 'wb'); pickle.dump(a_trial_all_score_contrast,f); f.close()
# f = open('a_trial_all_score_go.pckl', 'wb'); pickle.dump(a_trial_all_score_go,f); f.close()
# f = open('a_trial_all_score_reward.pckl', 'wb'); pickle.dump(a_trial_all_score_reward,f); f.close()


a_trial_all_score_contrast


#######################
#### KEEP! TO PLOT ####
#######################

# GOOD - Bernoulli Classification Simple (entire trial sequence)



trial_all_score_go={}
trial_all_score_reward={}


for j in range(len(alldat)):
    
    s_trial_all_score_go=[]
    s_trial_all_score_reward=[]
    
    for iregion in range(5):
        
        inf = sall_ML_states[j][iregion]
        if np.isnan(inf).any():
            
            dummy = np.empty(1); dummy[:] = np.nan
            s_trial_all_score_go.append(dummy)
            s_trial_all_score_reward.append(dummy)
            
            pass
        
        else:
            X, Y_dir, Y_reward = formatBern(sall_ML_states[j][iregion], sall_choice[j][iregion], sall_reward[j][iregion])
            all_score_go, all_score_reward = classifyBern2(X, Y_dir, Y_reward)

            s_trial_all_score_go.append(all_score_go)
            s_trial_all_score_reward.append(all_score_reward)


    trial_all_score_go[j] = s_trial_all_score_go
    trial_all_score_reward[j] = s_trial_all_score_reward

    
# # Save all as pickles
# f = open('trial_all_score_go.pckl', 'wb'); pickle.dump(trial_all_score_go,f); f.close()
# f = open('trial_all_score_reward.pckl', 'wb'); pickle.dump(trial_all_score_reward,f); f.close()


## Plot classification results

# Compile average score across sessions for each brain region, for each epoch type

br1={}
br2={}
br3={}
br4={}
br5={}
br6={}
br7={}
br8={}
br9={}


for iregion in range(5):

    tmp1 = []
    tmp2 = []
    tmp3 = []
    tmp4 = []
    tmp5 = []
    tmp6 = []
    tmp7 = []
    tmp8 = []
    tmp9 = []

    for j in range(len(alldat)):
        tmp1.append(np.mean(a_goslice_all_score_contrast[j][iregion])); tmp1a = np.array(tmp1);
        tmp2.append(np.mean(a_visslice_all_score_contrast[j][iregion])); tmp2a = np.array(tmp2);
        tmp3.append(np.mean(a_visslice_all_score_reward[j][iregion])); tmp3a = np.array(tmp3);
        tmp4.append(np.mean(a_visslice_all_score_go[j][iregion])); tmp4a = np.array(tmp4);
        tmp5.append(np.mean(a_trial_all_score_go[j][iregion])); tmp5a = np.array(tmp5);
        tmp6.append(np.mean(a_trial_all_score_reward[j][iregion])); tmp6a = np.array(tmp6)
        tmp7.append(np.mean(a_goslice_all_score_contrast[j][iregion])); tmp7a = np.array(tmp7)
        tmp8.append(np.mean(a_visslice_all_score_contrast[j][iregion])); tmp8a = np.array(tmp8)
        tmp9.append(np.mean(a_trial_all_score_contrast[j][iregion])); tmp9a = np.array(tmp9)
    
        
        br1[iregion]=tmp1a[~np.isnan(tmp1a)]
        br2[iregion]=tmp2a[~np.isnan(tmp2a)]
        br3[iregion]=tmp3a[~np.isnan(tmp3a)]
        br4[iregion]=tmp4a[~np.isnan(tmp4a)]
        br5[iregion]=tmp5a[~np.isnan(tmp5a)]
        br6[iregion]=tmp6a[~np.isnan(tmp6a)]
        br7[iregion]=tmp7a[~np.isnan(tmp7a)]
        br8[iregion]=tmp8a[~np.isnan(tmp8a)]
        br9[iregion]=tmp9a[~np.isnan(tmp9a)]


# # Save all as pickles
# f = open('br1_goslice_decode_reward.pckl', 'wb'); pickle.dump(br1,f); f.close()
# f = open('br2_goslice_decode_go.pckl', 'wb'); pickle.dump(br2,f); f.close()
# f = open('br3_visslice_decode_reward.pckl', 'wb'); pickle.dump(br3,f); f.close()
# f = open('br4_visslice_decode_go.pckl', 'wb'); pickle.dump(br4,f); f.close()
# f = open('br5_entiretrial_decode_go.pckl', 'wb'); pickle.dump(br5,f); f.close()
# f = open('br6_entiretrial_decode_reward.pckl', 'wb'); pickle.dump(br6,f); f.close()
# f = open('br7_goslice_decode_contrast.pckl', 'wb'); pickle.dump(br7,f); f.close()
# f = open('br8_visslice_decode_contrast.pckl', 'wb'); pickle.dump(br8,f); f.close()
# f = open('br9_entiretrial_decode_contrast.pckl', 'wb'); pickle.dump(br9,f); f.close()


# # # # #
# Plot Box-Whisker plots for decoding results based on epoch type
# # # # #

plt.figure(figsize=(15, 10))

plt.subplot(331)
plt.boxplot([br1[0],br1[1],br1[2],br1[3],br1[4]],
            labels = [regions[0],regions[1],regions[2],regions[3],regions[4]])
plt.ylim([0,1])
plt.title('Decoding Reward - Go Slice (HMM)')

plt.subplot(332)
plt.boxplot([br3[0],br3[1],br3[2],br3[3],br3[4]],
            labels = [regions[0],regions[1],regions[2],regions[3],regions[4]])
plt.ylim([0,1])
plt.title('Decoding Reward - Vis. Slice (HMM)')

plt.subplot(333)
plt.boxplot([br6[0],br6[1],br6[2],br6[3],br6[4]],
            labels = [regions[0],regions[1],regions[2],regions[3],regions[4]])
plt.ylim([0,1])
plt.title('Decoding Reward - Entire Trial (HMM)')



plt.subplot(334)
plt.boxplot([br2[0],br2[1],br2[2],br2[3],br2[4]],
            labels = [regions[0],regions[1],regions[2],regions[3],regions[4]])
plt.ylim([0,1])
plt.title('Decoding GO - Go Slice (HMM)')

plt.subplot(335)
plt.boxplot([br4[0],br4[1],br4[2],br4[3],br4[4]],
            labels = [regions[0],regions[1],regions[2],regions[3],regions[4]])
plt.ylim([0,1])
plt.title('Decoding GO - Vis. Slice (HMM)')

plt.subplot(336)
plt.boxplot([br5[0],br5[1],br5[2],br5[3],br5[4]],
            labels = [regions[0],regions[1],regions[2],regions[3],regions[4]])
plt.ylim([0,1])
plt.title('Decoding GO - Entire Trial (HMM)')


plt.subplot(337)
plt.boxplot([br7[0],br7[1],br7[2],br7[3],br7[4]],
            labels = [regions[0],regions[1],regions[2],regions[3],regions[4]])
plt.ylim([0,1])
plt.title('Decoding CONTRAST - Go Slice (HMM)')

plt.subplot(338)
plt.boxplot([br8[0],br8[1],br8[2],br8[3],br8[4]],
            labels = [regions[0],regions[1],regions[2],regions[3],regions[4]])
plt.ylim([0,1])
plt.title('Decoding CONTRAST - Vis. Slice (HMM)')

plt.subplot(339)
plt.boxplot([br9[0],br9[1],br9[2],br9[3],br9[4]],
            labels = [regions[0],regions[1],regions[2],regions[3],regions[4]])
plt.ylim([0,1])
plt.title('Decoding CONTRAST - Entire Trial (HMM)')
plt.tight_layout()


# plt.savefig('EventType_HMMSpikes_BrainRegions_Decoding_BoxWhisker.png')




