# "Snake plots" session 32
## Prep

# HIDE CELL
from importlib import reload

import matplotlib.pylab as plt
import numpy as np

import deps
reload(deps)

alldat, dat_LFP, dat_ST = deps.get_data()
dat = alldat[32]
sp, neu, trial_starts = deps.convert_raster_to_spiketimes(dat)
events_df = deps.convert_events_to_dataframe(dat, trial_starts,
                                             event_names=('gocue', 'response_time','feedback_time'),
                                             condition_names=('response','contrast_right','contrast_left','feedback_type'))

spykes_times = deps.spykes_get_times(sp, neu)



## Loop over all events and conditions
For every event, plot snakes and average PSTH for the population by condition


# HIDE CODE
for event in events_df.columns[events_df.columns.str.contains('time')]:
    for condition in events_df.columns[~events_df.columns.str.contains('time')]:
        pop, all_psth = deps.get_psth(spikes=spykes_times,
                               spykes_df=events_df,
                               event=event,
                               conditions=condition,
                               window=[-500, 1500],
                               bin_size=10,
                               )
        # Plot mean PSTH for all neurons
        plt.figure(figsize=[10,20])
        pop.plot_heat_map(all_psth, sortby='latency', sortorder='ascend', normalize='each', colors=['viridis'])  # or latency
        plt.figure(figsize=[10,10])
        pop.plot_population_psth(all_psth)