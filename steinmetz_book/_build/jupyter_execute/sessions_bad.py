# Hierarchical clustering for "bad" sessions


## Make spiketimes and events dataframe


# HIDE CODE
import deps
from importlib import reload
reload(deps)

alldat, dat_LFP, dat_ST = deps.get_data()


## Make and plot clusters

all_sessions=range(34)
sessions_good = [7, 8, 10, 11, 12, 14, 15, 16, 18, 20, 21, 24, 29, 30, 31, 32, 33, 34]
sessions_bad = set(all_sessions)-set(sessions_good)

# HIDE CODE
for i_session in sessions_bad:
    dat = alldat[i_session]
    sp, neu, trial_starts = deps.convert_raster_to_spiketimes(dat)
    events_df = deps.convert_events_to_dataframe(dat, trial_starts,
                                            event_names=('gocue', 'response_time','feedback_time'),
                                            condition_names=('response','contrast_right','contrast_left','feedback_type'))
    
    spykes_times = deps.spykes_get_times(sp, neu)
    for event_name in events_df.columns[events_df.columns.str.contains('time')]:
        # for condition_name in events_df.columns[~events_df.columns.str.contains('time')]:
        condition_name = 'feedback_type'#events_df.columns[~events_df.columns.str.contains('time')][0]
        print(f"======= Session {i_session}, event {event_name}  =======")
        try:
            _, all_psth = deps.get_psth(spikes=spykes_times,
                                   spykes_df=events_df,
                                   event=event_name, 
                                   conditions=condition_name,
                                   window=[-500, 1500], 
                                   bin_size=10,
                                   )
            # Make psth
            xar=deps.spyke2xar(all_psth)
    
            # Cluster
            df,df_tidy,index=deps.cluster(xar,plotose=False)
        except Exception as e:
            print(e)

