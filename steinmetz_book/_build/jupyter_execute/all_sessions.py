

# Hierarchical clustering for all sessions

if 0:
    # Hvplot
    #!pip install -U hvplot
    from bokeh.plotting import show
    import holoviews as hv
    import hvplot.pandas  # noqa
    %output size=320
    for i_sess, this_dat in enumerate(alldat):

        # Make spiketimes and events dataframe
        sp, neu, trial_starts = convert_raster_to_spiketimes(this_dat)
        events_df = convert_events_to_dataframe(this_dat,
                                                event_names=('gocue', 'response_time','feedback_time'),
                                                condition_names=('response','contrast_right','contrast_left','feedback_type'))
        spykes_times = spykes_get_times(sp, neu)

        # Clustering
        for event_name in events_df.columns[events_df.columns.str.contains('time')]:
            for condition_name in events_df.columns[~events_df.columns.str.contains('time')]:
                print(f"======= Session {i_sess} {event_name} / {condition_name} =======")
                _, all_psth = get_psth(spikes=spykes_times,
                                       spykes_df=events_df,
                                       event=event_name,
                                       conditions=condition_name,
                                       window=[-500, 1500],
                                       bin_size=10,
                                       )
                # Make psth
                xar=spyke2xar(all_psth)

                # Cluster
                df,df_tidy,index=cluster(xar,plotose=False)

                if df is None:
                    continue


                # Plot hvplot by condition
                plot=df.hvplot.heatmap(x='Time',y='Neuron',C=xar.name,row=condition_name,logz=False,
                                  flip_yaxis=True,cmap='Plasma',
                                  colorbar=False,symmetric=False,grid=False,xaxis=False,yaxis=False)
                                  #,datashade=True)
                show(hv.render(plot))