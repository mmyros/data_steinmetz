# @title Data retrieval

from os.path import expanduser as eu
import matplotlib.pylab as plt
import xarray as xr
import numpy as np


def get_data(fpath=eu('~/data/')):
    import os, requests
    fname = []
    for j in range(3):
        fname.append(fpath + 'steinmetz_part%d.npz' % j)
        url = ["https://osf.io/agvxh/download"]
        url.append("https://osf.io/uv3mw/download")
        url.append("https://osf.io/ehmw2/download")

    for j in range(len(url)):
        if not os.path.isfile(fname[j]):
            try:
                r = requests.get(url[j])
            except requests.ConnectionError:
                print("!!! Failed to download data !!!")
            else:
                if r.status_code != requests.codes.ok:
                    print("!!! Failed to download data !!!")
                else:
                    with open(fname[j], "wb") as fid:
                        fid.write(r.content)

    # @title Data retrieval for lfp and spike times

    fname = [fpath + 'steinmetz_st.npz']
    fname.append(fpath + 'steinmetz_wav.npz')
    fname.append(fpath + 'steinmetz_lfp.npz')

    url = ["https://osf.io/4bjns/download"]
    url.append("https://osf.io/ugm9v/download")
    url.append("https://osf.io/kx3v9/download")

    for j in range(len(url)):
        if not os.path.isfile(fname[j]):
            try:
                r = requests.get(url[j])
            except requests.ConnectionError:
                print("!!! Failed to download data !!!")
            else:
                if r.status_code != requests.codes.ok:
                    print("!!! Failed to download data !!!")
                else:
                    with open(fname[j], "wb") as fid:
                        fid.write(r.content)

    # @title Data loading
    import numpy as np

    alldat = np.array([])
    for j in range(len(fname)):
        alldat = np.hstack((alldat, np.load(fpath + 'steinmetz_part%d.npz' % j, allow_pickle=True)['dat']))

    # dat_LFP = np.load(fpath + 'steinmetz_lfp.npz', allow_pickle=True)['dat']
    # dat_ST = np.load(fpath + 'steinmetz_st.npz', allow_pickle=True)['dat']
    return alldat, None, None


# select just one of the recordings here. 11 is nice because it has some neurons in vis ctx.
# dat = alldat[11]


### Functions to work with spykes library

def get_psth(spikes, spykes_df, event, window=[-100, 100], bin_size=10, fr_thr=.1, **kwargs):
    """
    Calculates psth using spykes
    :param spikes:
    :param spykes_df:
    :param event:
    :param window:
    :param bin_size:
    :param fr_thr:
    :return:
    """
    import spykes
    import warnings
    warnings.filterwarnings('ignore')
    assert window[1] - window[0] > 0, 'Window size must be greater than zero!'
    # filter firing rate
    spikes = [i for i in spikes if i.firingrate > fr_thr]
    pop = spykes.plot.popvis.PopVis(spikes)
    # calculate psth
    mean_psth = pop.get_all_psth(event=event, df=spykes_df, window=window, binsize=bin_size, plot=False, **kwargs)
    # Check for sanity
    assert mean_psth['data'][0].size > 0, 'Empty group PSTH!'
    return pop, mean_psth


def spyke2xar(all_psth):
    """
    Psth to xarray
    :param all_psth: output of pop.get_all_psth
    :return: xarray
    """

    arr = np.stack([all_psth['data'][key] for key in all_psth['data'].keys() if len(all_psth['data'][key]) > 0])
    xar = xr.DataArray(arr,
                       dims=[all_psth['conditions'], 'Neuron', 'Time', ],
                       coords=[range(arr.shape[0]),
                               range(arr.shape[1]),
                               np.linspace(all_psth['window'][0],
                                           all_psth['window'][1], arr.shape[2]),
                               ],
                       name=all_psth['event']
                       )
    return xar


### Functions to get our data to spykes library

# @title Functions to get our data to spykes library

import pandas as pd


def convert_raster_to_spiketimes(dat, fs=100):
    """
    # Convert binned raster to spiketimes
    fs=100. # Sampling rate
    """

    # Make spiketime list
    trial_dur = dat['spks'].shape[2] / fs
    # Repeat t_trial n_trial times
    trial_starts = np.repeat(trial_dur, dat['spks'].shape[1])
    # cumsum
    trial_starts = np.cumsum(trial_starts)
    # starts with zero
    trial_starts -= trial_dur

    sp = [[np.where(r_trial)[0] / fs + trial_starts[i_trial]
           for i_trial, r_trial in enumerate(r_neuron)]
          for r_neuron in dat['spks']]  # list of spiketimes

    neu = np.hstack([np.repeat(i_neu, np.hstack(sp_n).shape[0]) for i_neu, sp_n in enumerate(sp)])
    sp = np.hstack([np.hstack(sp_n) for sp_n in sp])
    return sp, neu, trial_starts


def convert_events_to_dataframe(dat, trial_starts,
                                event_names=('gocue', 'response_time', 'feedback_time'),
                                condition_names=('response', 'contrast_right', 'contrast_left', 'feedback_type')):
    """
    Make events dataframe
    
    """
    events_df = []
    for event_name in event_names:
        df = pd.DataFrame({'time_' + event_name: (dat[event_name].squeeze().copy() + trial_starts)})
        for condition_name in condition_names:
            df[condition_name] = dat[condition_name].squeeze()
        events_df.append(df)

    events_df = pd.concat(events_df)
    events_df.rename({'time_feedback_time': 'time_feedback', 'time_response_time': 'time_response'}, axis=1,
                     inplace=True)
    events_df['response'] = events_df['response'] + 1
    events_df['feedback_type'] = events_df['feedback_type'] + 1
    return events_df


def spykes_get_times(s_ts, s_id, debug=False):
    """
    Use spykes library
    NB: Don't laugh, I wrote this a long time ago!
    :param s_ts:
    :param s_id:
    :param debug:
    :return:
    """

    def print_spyke(spykess):
        [print(len(spykess[i].spiketimes)) for i in range(len(spykess))]

    from spykes.plot import neurovis
    s_id = s_id.astype('int')
    neuron_list = list()
    for iu in np.unique(s_id):
        spike_times = s_ts[s_id == iu]
        if len(spike_times) < 2:
            if debug:
                print('Too few spiketimes in this unit: ' + str(spike_times))
            else:
                pass  # neuron_list.append(NeuroVis([],'ram'+str(iu)))
        else:
            neuron = neurovis.NeuroVis(spike_times, name='ram' + str(iu))
            neuron_list.append(neuron)

    if debug:
        print_spyke(neuron_list)
    return neuron_list


# use sns to Plot by condition
import seaborn as sns


def heatmap(df, **kwargs):
    """
    # This needs a pivoted dataframe already
    df.index[g.dendrogram_row.reordered_ind]
    Drawbacks:
    1. Doent do reordering apparently
    2. sns.heatmap has no faceting
    """
    import matplotlib.colors as colors
    from scipy.stats import zscore
    plt.figure(figsize=[10, 10])

    sns.heatmap(zscore(df, 0), robust=True,
                center=True,
                # norm=colors.SymLogNorm(0.03),
                **kwargs);


def cluster_old(xar, plotose=True, row_colors=None):
    def df_zscore(df, y='Power near', axis=('Subject',)):
        """
        zscore transform column of dataframe 'y' by "axis"
        :param df:
        :param y: string denoting column to transfor
        :param axis: can be list
        :return:
        """

        def nan_zscore(a, axis=0, ddof=0):
            """
            Copy of scipy.stats.zscore but works with nans
            Calculate the z score of each value in the sample, relative to the
            sample mean and standard deviation.

            Parameters
            ----------
            a : array_like
                An array like object containing the sample data.
            axis : int or None, optional
                Axis along which to operate. Default is 0. If None, compute over
                the whole array `a`.
            ddof : int, optional
                Degrees of freedom correction in the calculation of the
                standard deviation. Default is 0.

            Returns
            -------
            zscore : array_like
                The z-scores, standardized by mean and standard deviation of
                input array `a`.
            """
            a = np.asanyarray(a)
            mns = np.nanmean(a, axis=axis)
            sstd = np.nanstd(a, axis=axis, ddof=ddof)
            if axis and mns.ndim < a.ndim:
                return ((a - np.expand_dims(mns, axis=axis)) /
                        np.expand_dims(sstd, axis=axis))
            else:
                return (a - mns) / sstd


        from scipy.stats import zscore
        # df[y] = df.groupby(axis)[y].transform(lambda x: zscore(x,0))
        df[y] = df.groupby(axis)[y].transform(lambda x: zscore(x,0))
        return df

    value_name = xar.name
    condition_name = xar.dims[0]
    df = xar.to_dataframe().reset_index()
    # zscore
    df = df_zscore(df, value_name, 'Neuron')
    df = df.pivot(index=['Neuron', condition_name],
                  columns='Time',
                  values=value_name)
    print(df.values.min(), df.values.max())
    # from scipy.stats import zscore
    # df=df.reset_index()
    # print(df.shape)
    # print(zscore(df,0).shape)
    # df['values']=zscore(df,0)
    # Cluster conditions together, plot them (also together)
    try:
        g = sns.clustermap(df,
                           z_score=0,
                           figsize=[20, 40],
                           col_cluster=0,
                           # standard_scale=0,
                           metric="correlation",
                           center=True,
                           row_colors=row_colors, #TODO
                           robust=True)
    except Exception as e:
        print(e)
        # import pdb;pdb.set_trace()
        return None, None, None

    # reorder neurons according to Clustering
    index = df.index[g.dendrogram_row.reordered_ind]
    df = df.loc[index]

    plt.show()
    if plotose:
        # Plot conditions separately
        for condition in [0, 1]:
            heatmap(df.loc[index].xs(condition, level=condition_name));
            plt.title(f"{value_name} = {condition}")
            plt.show()

    # Back to normal dataframe
    df_tidy = df.copy()
    df = df.reset_index().melt(id_vars=['Neuron', condition_name], value_name=value_name)
    # df=df.set_index(list(set(df.columns)-{value_name}))

    # Categorical neuron prevents rearrangement 
    df['Neuron'] = df['Neuron'].astype('str')
    df['Time'] = df['Time'].astype('str')

    return df, df_tidy, index

def cluster(xar, plotose=True, row_colors=None):
    def df_zscore(df, y='Power near', axis=('Subject',)):
        """
        standardize (default)
        zscore transform column of dataframe 'y' by "axis"
        :param df:
        :param y: string denoting column to transfor
        :param axis: can be list
        :return:
        """

        def standardize(x):
            return (x - x.min()) / (x.max() - x.min())
        # Uncomment for actual zscore
        from scipy.stats import zscore
        df[y] = df.groupby(axis)[y].transform(lambda x: zscore(x,0))
        # df[y] = df.groupby(axis)[y].transform(lambda x: standardize(x))
        return df

    value_name = xar.name
    condition_name = xar.dims[0]
    df = xar.copy(deep=True).to_dataframe().reset_index()

    # zscore
    df = df_zscore(df, value_name, 'Neuron')

    df = df.pivot(index=['Neuron', condition_name],
                  columns='Time',
                  values=value_name)

    # Cluster conditions together, plot them (also together)
    try:
        g = sns.clustermap(df,
                           # z_score=False,
                           figsize=[10, 20],
                           col_cluster=0,
                           # standard_scale=0,
                           metric="correlation",
                           center=True,
                           row_colors=row_colors,  # TODO
                           robust=True)
    except Exception as e:
        print(e)
        # import pdb;pdb.set_trace()
        return None, None, None

    # reorder neurons according to Clustering
    index = df.index[g.dendrogram_row.reordered_ind]
    df = df.loc[index]

    plt.show()
    if plotose:
        # Plot conditions separately
        for condition in [0, 1]:
            heatmap(df.loc[index].xs(condition, level=condition_name));
            plt.title(f"{value_name} = {condition}")
            plt.show()

    # Back to normal dataframe
    df_tidy = df.copy()
    df = df.reset_index().melt(id_vars=['Neuron', condition_name], value_name=value_name)
    # df=df.set_index(list(set(df.columns)-{value_name}))


    # Categorical neuron prevents rearrangement
    df['Neuron'] = df['Neuron'].astype('str')
    df['Time'] = df['Time'].astype('str')

    return df, df_tidy, index
